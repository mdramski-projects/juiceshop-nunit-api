﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using APIServices.ReviewAPI.TestManagers;
using APIServices.UsersAPI.TestManagers;
using FluentAssertions;

namespace JuiceShop_NUnit_API.APITests
{
    public class ReviewTests
    {
        ReviewManager reviewManager;

        [SetUp]
        public void Setup()
        {
            // Arrange
            reviewManager = new ReviewManager();
        }

        [Test]
        public void Should_ReturnReviewsList_When_RequestingAllProductReview_As_User()
        {
            // Act
            var reviewsList = reviewManager.GetReviews(38);
            // Assert
            reviewsList.Data.Should().HaveCount(2);
            reviewsList.Data.Any(review => review.Author == "bender@juice-sh.op").Should().BeTrue();
            reviewsList.Data.Any(review => review.Message == "Puny mask for puny human weaklings!").Should().BeTrue();
        }

        [Test]
        public void Should_ReturnOkStatus_When_AddingReview_As_Admin()
        {
            // Act
            reviewManager.AddReview(38, "admin@juice-sh.op", "ThisOne");
            var reviewsList = reviewManager.GetReviews(38);
            // Assert
            reviewsList.Data.Where(review => review.Author == "admin@juice-sh.op").Should().NotBeNull();            
        }
    }
}
