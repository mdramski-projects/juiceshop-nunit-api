using APIServices.UsersAPI.Enums;
using APIServices.UsersAPI.TestManagers;
using FluentAssertions;

namespace RestSharpTests.APITests
{
    public class UsersTests
    {

        UsersManager userManager;

        [SetUp]
        public void Setup()
        {
            // Arrange
            userManager = new UsersManager();
        }

        [Test]
        public void Should_ReturnDefaultAdminOnList_When_ReturnAllUsers_As_Admin()
        {
            // Act
            var usersList = userManager.GetUsers();
            var mainAdmin = usersList.Data.Single(user => user.Email == "admin@juice-sh.op");
            // Assert
            mainAdmin.Should().NotBeNull();
        }

        [Test]
        public void Should_ReturnOneOrMoreAdmins_When_ReturnAllUsers_As_Admin()
        {
            // Act
            var usersList = userManager.GetUsers();
            var adminsList = usersList.Data.Where(user => user.Role == Role.admin.ToString());
            // Assert
            adminsList.Should().HaveCountGreaterThanOrEqualTo(1);
        }
    }
}