﻿using APIServices.ProductsAPI.Models;
using APIServices.ProductsAPI.TestManagers;
using FluentAssertions;

namespace RestSharpTests.APITests
{
    public class ProductsTests
    {

        ProductsManager productsManager;

        [SetUp]
        public void Setup()
        {
            // Arrange
            productsManager = new ProductsManager();
        }

        [Test]
        public void Should_ReturnAllProducts_When_RequestsProductsLists_As_User()
        {
            // Act
            var productsListResponse = productsManager.GetProducts();
            // Assert
            productsListResponse.Status.Should().Be("success");
            productsListResponse.Data.Count().Should().BeGreaterThanOrEqualTo(35);
        }

        [Test]
        public void Should_ReturnSpecificProduct_When_RequestsProductsLists_As_User()
        {
            // Act
            var productsListResponse = productsManager.GetProducts();
            // Assert
            productsListResponse.Status.Should().Be("success");
            var bananaJuice = productsListResponse.Data.FirstOrDefault(prod => prod.Name == "Banana Juice (1000ml)");
            bananaJuice.Should().NotBeNull("Because 'Banana Juice (1000ml)' should exist in the product list.");
        }

        [Test]
        public void Should_ReturnProduct_When_RequestsProductById_As_User()
        {
            // Arrange
            int productId = 1;
            string productName = "Apple Juice (1000ml)";
            // Act
            var productResponse = productsManager.GetProduct(productId);
            // Assert
            productResponse.Should().BeOfType<Product>("Because the response should be a valid Product object");
            productResponse.Name.Should().Be(productName, "Because we are expecting 'Apple Juice (1000ml)' product");
        }
    }
}