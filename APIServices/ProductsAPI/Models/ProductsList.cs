﻿namespace APIServices.ProductsAPI.Models
{
    public class ProductsList
    {
        public string Status { get; set; }
        public Product[] Data { get; set; }
    }
}
