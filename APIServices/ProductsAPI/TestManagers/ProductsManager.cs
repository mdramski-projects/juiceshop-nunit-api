﻿using System.Text.Json;
using APIServices.ProductsAPI.Models;
using RestSharp;

namespace APIServices.ProductsAPI.TestManagers
{
    public class ProductsManager
    {
        private const string _baseUrl = "http://localhost:3000/";
        private const string _endpoint = "api/Products";
        private RestClient _client;
        private RestResponse? _response;
        private RestRequest _request;

        public ProductsManager()
        {
            _client = new RestClient(_baseUrl);
        }

        public ProductsList GetProducts()
        {
            _request = new RestRequest(_endpoint, Method.Get);
            _response = _client.Execute(_request);

            string rawResponse = _response.Content;
            ProductsList juiceShopProducts = JsonSerializer.Deserialize<ProductsList>(rawResponse, new JsonSerializerOptions
            {
                PropertyNameCaseInsensitive = true
            });
            return juiceShopProducts;
        }

        public Product GetProduct(int productId)
        {
            string endpoint = $"/api/products/{productId}";
            _request = new RestRequest(endpoint, Method.Get);
            _response = _client.Execute(_request);

            string rawResponse = _response.Content;

            using (JsonDocument doc = JsonDocument.Parse(rawResponse))
            {
                JsonElement root = doc.RootElement;
                JsonElement data = root.GetProperty("data");

                Product product = JsonSerializer.Deserialize<Product>(data.GetRawText(), new JsonSerializerOptions
                {
                    PropertyNameCaseInsensitive = true
                });

                return product;
            }
        }
    }
}
