﻿using System.ComponentModel;

namespace APIServices.ReviewAPI.Models
{
    public class Review
    {
        public string Author { get; set; }
        public string Message { get; set; }
        public int Product { get; set; }
        public int LikesCount { get; set; }

        public string[] LikedBy { get; set; }

        public string _Id { get; set; }
    }
}
