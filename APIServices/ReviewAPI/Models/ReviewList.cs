﻿namespace APIServices.ReviewAPI.Models
{
    public class ReviewList
    {
        public string Status { get; set; }
        public Review[] Data { get; set; }
    }
}