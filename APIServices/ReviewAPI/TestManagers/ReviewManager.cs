﻿using System.Text.Json;
using APIServices.ProductsAPI.Models;
using APIServices.ReviewAPI.Models;
using APIServices.TokenAPI.TestManagers;
using APIServices.UsersAPI.Models;
using RestSharp;

namespace APIServices.ReviewAPI.TestManagers
{
    public class ReviewManager
    {
        private const string _baseUrl = "http://localhost:3000/";
        private const string _endpoint = "http://localhost:3000/rest/products/1/reviews";
        private RestClient _client;
        private RestResponse? _response;
        private RestRequest _request;

        public ReviewManager()
        {
            _client = new RestClient(_baseUrl);
        }

        public ReviewList GetReviews(int productId)
        {
            string endpoint = $"/rest/products/{productId}/reviews";
            _request = new RestRequest(endpoint, Method.Get);
            _response = _client.Execute(_request);

            string rawResponse = _response.Content;
            ReviewList reviews = JsonSerializer.Deserialize<ReviewList>(rawResponse, new JsonSerializerOptions
            {
                PropertyNameCaseInsensitive = true
            });
            return reviews;
        }
        
        public void AddReview(int productId, string author, string message)
        {
            string endpoint = $"/rest/products/{productId}/reviews";
            var tokenHandler = new TokenHandler();
            var token = tokenHandler.GetToken();
            _request = new RestRequest(endpoint, Method.Put);
            _request.AddHeader("Authorization", "Bearer " + token);
            _request.AddJsonBody(new { author, message });
            _response = _client.Execute(_request);
        }
    }
}
