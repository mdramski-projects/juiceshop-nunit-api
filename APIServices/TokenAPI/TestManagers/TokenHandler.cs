﻿using System.Text.Json;
using RestSharp;

namespace APIServices.TokenAPI.TestManagers
{
    public class TokenHandler
    {
        private const string _baseUrl = "http://localhost:3000/";
        private const string _endpoint = "rest/user/login";
        private RestClient _client;
        private RestResponse? _response;
        private RestRequest _request;
        private const string AdminUser = "admin@juice-sh.op";
        private const string AdminPassword = "admin123";

        public TokenHandler()
        {
            _client = new RestClient(_baseUrl);
        }

        public string? GetToken()
        {
            _request = new RestRequest(_endpoint, Method.Post);
            _request.AddHeader("Content-Type", "application/json");

            var loginData = new
            {
                email = AdminUser,
                password = AdminPassword
            };

            string json = JsonSerializer.Serialize(loginData);
            _request.AddParameter("application/json", json, ParameterType.RequestBody);
            _response = _client.Execute(_request);
            JsonDocument doc = JsonDocument.Parse(_response.Content);
            var token = doc.RootElement.GetProperty("authentication").GetProperty("token").GetString();
            return token;
        }
    }
}
