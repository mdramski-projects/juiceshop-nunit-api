﻿namespace APIServices.UsersAPI.Enums
{
    public enum Role
    {
        admin,
        customer,
        deluxe,
        accounting
    }
}
