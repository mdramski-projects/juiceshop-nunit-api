﻿using System.Text.Json;
using APIServices.TokenAPI.TestManagers;
using APIServices.UsersAPI.Models;
using RestSharp;

namespace APIServices.UsersAPI.TestManagers
{
    public class UsersManager
    {        
        private const string _baseUrl = "http://localhost:3000/";
        private const string _endpoint = "api/Users";
        private RestClient _client;
        private RestResponse? _response;
        private RestRequest _request;

        public UsersManager()
        {
            _client = new RestClient(_baseUrl);
        }

        public JuiceShopUsers GetUsers()
        {
            var tokenHandler = new TokenHandler();
            var token = tokenHandler.GetToken();
            _request = new RestRequest(_endpoint, Method.Get);
            _request.AddHeader("Authorization", "Bearer " + token);
            _response = _client.Execute(_request);

            string rawResponse = _response.Content;
            JuiceShopUsers juiceShopUsers = JsonSerializer.Deserialize<JuiceShopUsers>(rawResponse, new JsonSerializerOptions
            {
                PropertyNameCaseInsensitive = true
            });
            return juiceShopUsers;
        }
    }
}
