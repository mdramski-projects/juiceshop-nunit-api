﻿namespace APIServices.UsersAPI.Models
{
    public class JuiceShopUser
    {
        public int Id { get; set; }
        public string UserName { get; set; }
        public string Email { get; set; }
        public string Role { get; set; }
        public string DeluxeToken { get; set; }
        public string LastLoginIp { get; set; }
        public string ProfileImage { get; set; }
        public bool IsActive { get; set; }
        public DateTimeOffset CreatedAt { get; set; }
        public DateTimeOffset UpdatedAt { get; set; }
        public DateTimeOffset? DeletedAt { get; set; }

    }
}