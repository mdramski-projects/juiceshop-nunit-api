﻿namespace APIServices.UsersAPI.Models
{
    public class JuiceShopUsers
    {
        public string Status { get; set; }
        public JuiceShopUser[] Data { get; set; }
    }
}
